# DevOps Repo: A space for multiple items
[![pipeline status](https://gitlab.com/scoday1/devops/badges/master/pipeline.svg)](https://gitlab.com/scoday1/devops/commits/master)

## Introduction
The concept of DevOps for me first appeared in 2008, as hard as that may be to believe. 
We did not have the automation as we do today but puppet was in use, rpm build was used,
and the concept of a reference master was in play.

## Simple Nginx Auth
This is just an easy way to add password foo to nginx.

`zypper/apt/yum/dnf install apache2-util`   
`htpasswd -c /etc/nginx/htpasswd.users $username`   

### Add the entry into nginx.conf
There are two lines to add into your nginx.   
`auth_basic "Sometitle";`   
and   
`auth_basic_user_file /etc/nginx/htpasswd.users;`   

Together, or inside a proxy setup it looks like below:   

    server {   
      listen 80;   
      server_name site.foo.jp;   
      access_log    /var/log/nginx/site.foo.log;   
      error_log     /var/log/nginx/site.error.foo.log;   
      
      location / {   
      
          auth_basic "Kibana: The wave";   
          auth_basic_user_file /etc/nginx/htpasswd.users;   
   
          proxy_set_header Host $host;   
          proxy_set_header X-Real-IP $remote_addr;   
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;   
          proxy_set_header X-Forwarded-Proto $scheme;   
   
      # Fix the "It appears that your reverse proxy setup is broken" error.   
          proxy_pass         http://1.1.1.1:5601;   
          proxy_read_timeout 30;   
      }   
    }
