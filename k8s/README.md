# DevOps Repo: A space for multiple items
## K8S Specific items to get up and running.

## Introduction
This is a spot for k8s specific things and stuff. Basically this will be part of
a CI/CD pipeline at some point. 

## Simple microk8s Setup
Snap: `snap isntall microk8s --classic`

## Deploy ISTIO
Snap: `sudo microk8s.enable istio`

