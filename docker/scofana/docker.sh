docker run \
-d \
-v /scofana/:/var/lib/grafana \
-p 3000:3000 \
-v /docker/scofana:/var/lib/grafana
--name=ScoFana01 \
-e "GF_SERVER_ROOT_URL=http://localhost" \
-e "GF_SECURITY_ADMIN_USER=admin" \
-e "GF_SECURITY_ADMIN_PASSWORD=TOPSECRET" \
scofana:latest
