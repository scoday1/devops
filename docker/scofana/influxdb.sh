

$ docker run -p 8083:8083 -p 8086:8086
-v $PWD:/var/lib/influxdb
influxdb Modify $PWD to the directory where you want to store data associated with the InfluxDB container.

You can also have Docker control the volume mountpoint by using a named volume.

$ docker run -p 8083:8083 -p 8086:8086
-v influxdb:/var/lib/influxdb
influxdb
